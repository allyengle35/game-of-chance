var answers = ["It is certain",
    "It is decidedly so",
    "Without a doubt",
    "Yes - definitely",
    "You may rely on it",
    "As I see it, yes",
    "Most likely",
    "Outlook good",
    "Yes", "Signs point to yes",
    "Don't count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not so good",
    "Very doubtful",
    "Reply hazy, try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",
    "Not in this lifetime",
    "You know better than that!",
    "Try again in 37.333 seconds",
    "It will happen tomorrow at 3:15 pm at the south end of Euston Station...unless it's raining",
    "Your spirit guides say to stop bothering them",
    "Banana, Banana",
    "Maybe, but not if you keep worrying about it",
    "Probably, if you're good",
    "Yes, with reservations",
    "Not likely, Mate",
    "Shame on you!"
];

window.onload = function() {
    var eight = document.getElementById("eight");
    var answer = document.getElementById("answer");
    var eightball = document.getElementById("eight-ball");
    var question = document.getElementById("question");

    eightball.addEventListener("click", function() {
        if (question.value.length < 1) {
            alert('Enter a question!');
        } else {
            eight.innerText = "";
            var num = Math.floor(Math.random() * Math.floor(answers.length));
            answer.innerText = answers[num];
        }
    });
};